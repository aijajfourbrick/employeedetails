let ejs = require('ejs')
// let crypto = require("crypto");
let user = require("../schemas/user");
// const { constants } = require('buffer');
const path=require('path')

const create= (req,res)=>{
    console.log("api is working...")
    res.sendFile(path.join(__dirname, '../' + '/view/create.html'))
}

const save=(req,res)=>{
    var obj = new user(req.body);
    obj.save()
    .then((data)=>{
        console.log(data)
        res.send("User inseted Successfully!")
    })
    .catch((err)=>{
        console.log(err)
        message: err.message || "Some error occurred while creating the User."
    })
}

const find= (req,res)=>{
    user.findById(req.params.user_id)
    .then((result)=>{
        if(result){
            console.log(result)
            res.send(result)
            // ejs.renderFile('templates/shoppingcart.ejs',{result},(err,data)=>{
        }else{
            console.log("not found employee of this id")
            res.send("not fount of thid data")
        }
    })
    .catch((err)=>{
        console.log(err)
    })
}


const findall= (req,res)=>{
    user.find()
    .then((result)=>{
        // console.log(result)
        if(!result){
            console.log(err)
        }else{
            console.log(result)
            // res.send(result)
			res.render( '../' + 'template/all_details.ejs', {data: result})
        }
    })
    .catch((err)=>{
        console.log(err)
    })
}


const update=(req,res)=>{
    var users=req.body
    user.findByIdAndUpdate({_id:req.params.user_id}, 
        {$set:{
            email: users.email, 
            name: users.name, 
            profile: users.profile, 
            phone: users.phone, 
            address: users.address
        }
    })
    .then((result)=>{
        console.log(result)
        res.send({status: "update data successfully! ", "result": result})
    })
    .catch((err)=>{
        console.log(err)
    })
}



const deleted=(req,res)=>{
    user.findByIdAndDelete(req.params.user_id)
    .then((result)=>{
        console.log("result")
        res.status(200).send({message:"successful"});
    })
    .catch((err)=>{
        console.log(err)
    })
}



module.exports={
    update,
    save,
    find,
    findall,
    deleted,
    create
}