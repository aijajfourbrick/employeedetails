
const express=require('express')
const validator = require("express-joi-validation").createValidator({}); //new thing
const querySchema=require('../validation/validate')
const crud=require('../controller/user')
const router=express()

router.post("/addUser", validator.body(querySchema),crud.save);

router.get('/create', crud.create)
router.post('/save', crud.save )
router.get('/getAll', crud.findall)
router.get('/getAll/:user_id', crud.find)
router.put('/edit/:user_id', crud.update)
router.delete('/delete/:user_id', crud.deleted)

module.exports={
    router
}