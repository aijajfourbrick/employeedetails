const mongoose = require('mongoose');
require('dotenv').config()
// url=process.env.local_url
mongoose.connect(`mongodb://localhost:27017/fourbrick`, { 
    useUnifiedTopology: true,
    useNewUrlParser:true
} ,(err,db)=>{
    if (!err) {
        console.log('Success')
    }
    else {
        console.log('Failed'+ err)
    }
});

module.exports = mongoose;
