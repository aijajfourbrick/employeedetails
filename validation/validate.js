const Joi = require("joi");

const querySchema = Joi.object({
  name: Joi.string().required(),
  prfile:Joi.string().required(),
  address:Joi.string().required(),
  phone:Joi.number().integer().required(),
  image: Joi.string()
});

module.exports = querySchema;
