const { Schema } = require('../database/db')
const mongoose=require('../database/db')

let employeeSchema= new mongoose.Schema({
    email: String,
    name: String,
    profile: String,
    phone: Number,
    address: String,
    image: String
})

module.exports=mongoose.model("User", employeeSchema);