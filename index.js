var express = require("express");
var bodyParser = require("body-parser");
//var path = require("path")
let router=require('./routes/routes')

var app = express();
app.use(express.static("public"));
app.use(bodyParser.json());
app.set('view engine', 'ejs'); 
app.engine("html", require("ejs").renderFile);  //new things
/**
 * parse requests of content-type - application/x-www-form-urlencoded
 */
app.use(bodyParser.urlencoded({ extended: true }));

app.use('/',router.router)

app.listen(3000);
console.log("Server Listening on port 3000");
